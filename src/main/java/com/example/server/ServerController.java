package com.example.server;

import com.alibaba.fastjson.JSONException;
import com.example.server.util.StringUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author ht000182
 */
public class ServerController {
    @FXML
    public TextField tfPort;
    private boolean flag = true;
    @FXML
    private TextArea textFile1;
    @FXML
    private TextArea textArea;
    private PrintWriter out;
    private String rcvStrBuff;

    @FXML
    public void onStartClick() {

        if (flag) {
            new SocketThread(textArea,tfPort.getText());
        } else {
            textArea.appendText("INFO: 服务已启动\n");
        }
    }

    @FXML
    public void onSendClick() {
        if (out != null) {
            String text = textFile1.getText();
            out.println(text.replaceAll("\\s*", ""));
            textArea.appendText("Server:   " + text + "\n");
        } else {
            textArea.appendText("ERROR: 没有客户端接入\n");
        }

    }

    @FXML private javafx.scene.control.Button closeButton;
    @FXML
    private void closeButtonAction(){
        System.out.println("sssssss");
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }


    public void onClientClick() throws IOException {
        Stage anotherStage = new Stage();
        Parent anotherRoot = FXMLLoader.load(getClass().getResource("client-view.fxml"));
        Scene anotherScene = new Scene(anotherRoot);
        anotherStage.setTitle("客户端");
        anotherStage.getIcons().add(new Image("logo.png"));
        anotherStage.setX(500);
        anotherStage.setScene(anotherScene);
        anotherStage.show();
    }

    public void onFormatJson() {
        textFile1.setText(StringUtils.formatJson(textFile1.getText()));
    }

    public void onGetJsonClick(ActionEvent actionEvent) {
        try{
            textFile1.setText(StringUtils.formatJson(rcvStrBuff));
        }catch (JSONException e){

        }
    }

    class SocketThread extends Thread {
        private final TextArea textArea;
        private int port;
        public SocketThread(TextArea textArea,String port) {
            this.port=Integer.parseInt(port);
            this.textArea = textArea;
            start();
        }

        @Override
        public void run() {
            try (ServerSocket serverSocket = new ServerSocket(port)) {
                ServerController.this.textArea.appendText("INFO: 服务启动" + serverSocket + "\n");
                flag = false;
                while (true) {
                    new SendThread(textArea, serverSocket.accept());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class SendThread extends Thread {
        private final TextArea textArea;
        private final Socket socket;

        public SendThread(TextArea textArea, Socket socket) {
            this.socket = socket;
            this.textArea = textArea;
            start();
        }

        @Override
        public void run() {
            try {
                new ReceiveThread(ServerController.this.textArea, socket);
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(),"utf-8")), true);
            } catch (IOException e) {
                textArea.appendText("ERROR: " + e + "\n");
                e.printStackTrace();
            }
        }
    }

    class ReceiveThread extends Thread {
        BufferedReader in = null;
        private final TextArea textArea;
        private final Socket socket;

        public ReceiveThread(TextArea textArea, Socket socket) {
            this.textArea = textArea;
            this.socket = socket;
            start();
        }

        @Override
        public void run() {
            try {
                textArea.appendText("INFO: 客户端已连接" + socket + "\n");
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                while (true) {
                    rcvStrBuff = in.readLine();
                    textArea.appendText("Client:   " + rcvStrBuff + "\n");
                    if ("END".equals(rcvStrBuff)) {
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

