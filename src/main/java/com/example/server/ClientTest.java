package com.example.server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
/**
 * @author ht000182
 * @date 2022/3/31 15:21
 */
public class ClientTest {
    public static void main(String[] args) {
        int j = 0;
        try (Socket socket = new Socket("127.0.0.1", 8088);
             PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true)) {
            while (true) {
                j++;
                Thread.sleep(2000);
                out.println("TESt");
                if (j > 100) {
                    break;
                }
            }
        } catch (IOException | InterruptedException e) {
            Thread.currentThread().interrupt();
        }

    }
}
