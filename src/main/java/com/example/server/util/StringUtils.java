package com.example.server.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @author LuoYuSanQian
 * @version 1.0
 * @date 2022/4/7 18:24
 */
public class StringUtils {


    public static String formatJson(String strjson) {
        JSONObject object = JSONObject.parseObject(strjson);
        return JSON.toJSONString(object, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
                SerializerFeature.WriteDateUseDateFormat);
    }

}
